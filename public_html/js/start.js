/**
 * Start script
 * 
 * @author Yevhen Matasar <matasar.ei@gmail.com>
 */

$(window).on('load', function () {
    initMap();
    initMenuTop();
});

var initMenuTop = function() {
    $('#menu_top_btn').click(function() {
        $('#menu_top_container').toggleClass('show-mobile');
    });
    
};

/**
 * @returns {undefined}
 */
var updateMap = function() {
    var windowHeight = $(window).height();
    var bodyHeight = $('body').outerHeight();
    
    if (windowHeight - bodyHeight < windowHeight * 0.4) {
        
        $('#map').height(windowHeight * 0.6);
        return;
    }
    
    $('#map').height(windowHeight - bodyHeight);
};

/**
 * @returns {undefined}
 */
var initMap = function() {
    if ($('#map').length === 0) {
        return;
    }
    
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 50.450808, lng: 30.523048},
        zoom: 17,
        styles: [
            {
                "featureType": "administrative",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": "-100"
                    }
                ]
            },
            {
                "featureType": "administrative.province",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 65
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": "50"
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": "-100"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "all",
                "stylers": [
                    {
                        "lightness": "30"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "all",
                "stylers": [
                    {
                        "lightness": "40"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "hue": "#ffff00"
                    },
                    {
                        "lightness": -25
                    },
                    {
                        "saturation": -97
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels",
                "stylers": [
                    {
                        "lightness": -25
                    },
                    {
                        "saturation": -100
                    }
                ]
            }
        ]
    });
    
    $(window).resize(function() {
        updateMap(); 
    }).trigger('resize');
};